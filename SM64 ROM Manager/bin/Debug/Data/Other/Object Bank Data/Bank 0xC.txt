;Bank 0xC

[Haunted Objects]
List = Bookend|Haunted Chair|Small Key|Mad Piano|Boo|Haunted Cage
cmd17 = 17 0C 01 05 00 91 BE B0 00 92 C0 30|17 0C 00 0C 00 16 D5 C0 00 16 D8 70
cmd06 = 06 08 00 00 15 00 08 4C

[Snow Objects]
List = Spindrift|Penguin|Mr. Blizzard
cmd17 = 17 0C 01 05 00 8F B8 B0 00 90 89 E0|17 0C 00 0C 00 16 56 E0 00 16 5A 50
cmd06 = 06 08 00 00 15 00 08 0C

[Assorted Enemies (1)]
List = Monty Mole|Ukiki Monkey|Fwoosh
cmd17 = 17 0C 01 05 00 8D D7 F0 00 8F 38 B0|17 0C 00 0C 00 16 02 E0 00 16 06 70
cmd06 = 06 08 00 00 15 00 07 E8

[Desert Objects]
List = Klepto|Eyerock|Pokey
cmd17 = 17 0C 01 05 00 8C 11 A0 00 8D 57 F0|17 0C 00 0C 00 15 1B 70 00 15 21 D0
cmd06 = 06 08 00 00 15 00 07 B4

[Big Bob-Omb Boss]
List = Big Bob-Omb|Water Bubble
cmd17 = 17 0C 01 05 00 88 C3 D0 00 89 D4 70|17 0C 00 0C 00 13 B5 D0 00 13 B9 10
cmd06 = 06 08 00 00 15 00 07 6C

[Assorted Enemies (2)]
List = Yellow Sphere|Hoot the Owl|Yoshi Egg|Thwomp|Bullet Bill|Heave-Ho
cmd17 = 17 0C 01 05 00 86 0E F0 00 87 62 50|17 0C 00 0C 00 13 28 50 00 13 2C 60
cmd06 = 06 08 00 00 15 00 07 1C

[Water Objects]
List = Clam|Shark|Unagi
cmd17 = 17 0C 01 05 00 8A 54 70 00 8B 91 A0|17 0C 00 0C 00 14 5C 10 00 14 5E 90
cmd06 = 06 08 00 00 15 00 07 88

[Assorted Enemies (3)]
List = Bubba|Wiggler|Lakitu|Spinny Ball|Spinny
cmd17 = 17 0C 01 05 00 96 02 30 00 97 70 F0|17 0C 00 0C 00 18 7F A0 00 18 84 40
cmd06 = 06 08 00 00 15 00 08 A4

[Peach & Yoshi]
List = Birds|Peach|Yoshi
cmd17 = 17 0C 01 05 00 93 40 30 00 95 82 30|17 0C 00 0C 00 18 05 40 00 18 0B B0
cmd06 = 06 08 00 00 15 00 08 88

[Switches]
List = Beta Trampoline|Cap Switches
cmd17 = 17 0C 01 05 00 91 09 E0 00 91 3E B0|17 0C 00 0C 00 16 6B D0 00 16 6C 60
cmd06 = 06 08 00 00 15 00 08 30

[Lava Objects]
List = Small Bully|Big Bully|Beta Blargg
cmd17 = 17 0C 01 05 00 87 E2 50 00 88 43 D0|17 0C 00 0C 00 13 4A 70 00 13 4D 20
cmd06 = 06 08 00 00 15 00 07 50
