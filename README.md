# SM64 ROM Manager
A brand new all-in-one SM64 Hacking tool that allows you to import and modify custom levels, edit texts, music and many more!

## Download & Compile
Let me explain how to get tbe able to compile the source code.
 1. Download the source code
 2. Download the latest version of SM64 ROM Manager.
 3. Go inside the "Shared Libs" folder in the repo folder: "SM64 ROM Manager\Shared Libs\DotNetBarNew"
 4. Double-Click the registry file "DotNetBar_License.reg" to instert it to your registry.
 This will install a working license for DotNetBar for SM64 ROM Manager development.
 5. Open the project solution.
 6. Restore the NuGet packages for the whole package.
 There is an option for that in Visual Studio by right-click on the project solution and click to "Restore NuGet packages".
 7. Done!

Now you should be able to compile the source code without problems for private use.

## Requiements
 - Visual Studio 2017 or newer
 - .Net Framework 4.5
 - Internet connection (for restoring NuGet packages)
