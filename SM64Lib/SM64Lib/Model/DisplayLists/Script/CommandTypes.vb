﻿Namespace Global.SM64Lib.Model.Fast3D.DisplayLists.Script

    Public Enum CommandTypes
        F3D_NOOP = &H0
        F3D_Movemem = &H3
        F3D_DisplayList = &H6
        F3D_EndDisplaylist = &HB8
        F3D_Vertex = &H4
        F3D_Triangle1 = &HBF
        F3D_ClearGeometryMode = &HB6
        F3D_SetGeometryMode = &HB7
        G_Loadtlut = &HF0
        G_SetTileSize = &HF2
        G_SetImage = &HFD
        G_Loadback = &HF3
        G_SetTile = &HF5
        G_Texture = &HBB
    End Enum

End Namespace