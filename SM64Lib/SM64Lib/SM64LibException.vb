﻿Namespace Global.SM64Lib.Exceptions

    Public Class RomCompatiblityException
        Inherits Exception

        Public Sub New(msg As String)
            MyBase.New(msg)
        End Sub

    End Class

End Namespace
