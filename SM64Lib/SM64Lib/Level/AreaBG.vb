﻿Imports System.Drawing

Namespace Global.SM64Lib.Levels

    Public Class AreaBG

        Public Property Type As AreaBGs = AreaBGs.Levelbackground
        Public Property Color As Color = Color.Black

    End Class

    Public Enum AreaBGs
        Levelbackground
        Color
    End Enum

End Namespace
