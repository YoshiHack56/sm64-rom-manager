﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports IniParser.Model
Imports SM64Lib.Patching
Imports SM64Lib.Model
Imports System.Runtime.InteropServices
Imports System.Drawing.Drawing2D
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection

Namespace Global.SM64Lib

    Public Class RGBColor
        Public Red As Byte = 0
        Public Green As Byte = 0
        Public Blue As Byte = 0
    End Class

End Namespace
