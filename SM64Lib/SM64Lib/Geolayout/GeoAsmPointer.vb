﻿Imports System.IO
Imports SM64Lib.Geolayout.Script, SM64Lib.Geolayout.Script.Commands
Imports SM64Lib.Levels

Namespace Global.SM64Lib.Geolayout

    Public Enum GeoAsmPointer
        EnvironmentEffect = &H802761D0
        Water = &H802D104C
        Unknown = &H802CD1E8
    End Enum

End Namespace
