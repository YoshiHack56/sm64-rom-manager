﻿Imports System.IO
Imports SM64Lib.Geolayout.Script, SM64Lib.Geolayout.Script.Commands
Imports SM64Lib.Levels

Namespace Global.SM64Lib.Geolayout

    Public Enum BackgroundIDs As Byte
        HauntedForest = &H6
        SnowyMountains = &H4
        Desert = &H5
        Ocean = &H0
        UnderwaterCity = &H2
        BelowClouds = &H8
        AboveClouds = &H3
        Cavern = &H7
        FlamingSky = &H1
        PurpleClouds = &H9
        Custom = &HA
    End Enum

End Namespace
